﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
namespace CronometroBio
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            timer1.Start();
            timer3.Start();
            NoFolio.Text = Globales.nofolio.ToString();
            Folio.Text = Globales.nofolio.ToString();
        }

        //variables globales
         static class Globales
        {
            public static bool start1 = false;
            public static bool start2 = false;
            public static bool start3 = false;
            public static bool contadorfinal2 = false;
            public static int contadorfinal = 0;
            public static int minutosfinales = 0;
            public static int mins = 1;  //(limite de minutos) -1
            public static int secs = 60;  //(limite de segundos) +1
            public static string ruta = " ";
            public static int nofolio = 1; //numero de paciente
            
        }
        private void HoraActual_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dateTime2 = DateTime.Now;
            DateTime dateTime  = DateTime.Now;
            this.HoraActual.Text = dateTime.ToLongTimeString();
            this.FechaActual.Text = dateTime2.ToLongDateString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            IP.Text = HoraActual.Text;
            timer3.Start();
            Globales.contadorfinal2 = true;
            Globales.contadorfinal = 0;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AE.Text = HoraActual.Text;
            

        }
      
        public void start1_Click(object sender, EventArgs e)
        {
            PSP.Text = AE.Text;   // Es igual al tiempo que Arteria encontrada
            timer2.Enabled = true;
            Globales.start1 = true;
            PSP2.Text = "Tiempo no tomado";

        }
       
     
       
        public void timer2_Tick(object sender, EventArgs e)
        {
         //--------------------------------------------------------------------      
           if (Globales.start1)
           {
               timer2.Interval = 890;  //intervalo para el temporizador 
               Globales.secs.ToString();
               sec.Text = Globales.secs.ToString();
               min.Text = Globales.mins.ToString();
               Globales.secs--;
               if (Globales.secs <= 9 && Globales.secs >= 0)  // si esta dentro de los valores de 0 y 9
               {

                   sec.Text = "0" + Globales.secs.ToString();

               }
               else
               {
                   sec.Text = Globales.secs.ToString();
                   min.Text = Globales.mins.ToString();

               }

               if (Globales.secs == 0)  //si el limite  de segundos es 0
               {
                   Globales.mins--;
                   Globales.secs = 60; //cambiar limite de segundos 
               }

               if (Globales.mins == -1)  //si se llega a 0 los minutos 
               {
                   timer2.Stop();
                   Globales.mins = 1;  //(limite de minutos) -1
                   Globales.secs = 60;  //(limite de segundos) +1
                   //sec.Text = "0" + secs.ToString();
                   sec.Text = Globales.secs.ToString();
                   min.Text = Globales.mins.ToString();
                   //Aqui se activa la alarma 
                   SoundPlayer player = new SoundPlayer();
                   //Aqui va el directorio del wav 
                   player.SoundLocation = "alert.wav";
                   player.Load();
                   player.PlaySync();

                   PSP2.Text = HoraActual.Text;
                   Globales.start1 = false;
               }

            }
          //-----------------------------------------------------------------
           if (Globales.start2)
           {
               timer2.Interval = 890;  //intervalo para el temporizador 
               sec.Text = Globales.secs.ToString();
               min.Text = Globales.mins.ToString();
               Globales.secs--;
               if (Globales.secs <= 9 && Globales.secs >= 0)  // si esta dentro de los valores de 0 y 9
               {

                   sec.Text = "0" + Globales.secs.ToString();

               }
               else
               {
                   sec.Text = Globales.secs.ToString();
                   min.Text = Globales.mins.ToString();

               }

               if (Globales.secs == 0)  //si el limite  de segundos es 0
               {
                   Globales.mins--;
                   Globales.secs = 60; //cambiar limite de segundos 
               }

               if (Globales.mins == -1)  //si se llega a 0 los minutos 
               {
                   timer2.Stop();
                   Globales.mins = 1;  //(limite de minutos) -1
                   Globales.secs = 60;  //(limite de segundos) +1
                   // sec.Text = "0" + secs.ToString();
                   sec.Text = Globales.secs.ToString();
                   min.Text = Globales.mins.ToString();
                   //Aqui se activa la alarma 
                   SoundPlayer player = new SoundPlayer();
                   //Aqui va el directorio del wav 
                   player.SoundLocation = "alert.wav";
                   player.Load();
                   player.PlaySync();

                   PCP2.Text = HoraActual.Text;
                   Globales.start2 = false;
               }

           }
           //-----------------------------------------------------------------
           //--------------------------------------------------------------------      
           if (Globales.start3)
           {
               timer2.Interval = 890;  //intervalo para el temporizador 
               sec.Text = Globales.secs.ToString();
               min.Text = Globales.mins.ToString();
               Globales.secs--;
               if (Globales.secs <= 9 && Globales.secs >= 0)  // si esta dentro de los valores de 0 y 9
               {

                   sec.Text = "0" + Globales.secs.ToString();

               }
               else
               {
                   sec.Text = Globales.secs.ToString();
                   min.Text = Globales.mins.ToString();

               }

               if (Globales.secs == 0)  //si el limite  de segundos es 0
               {
                   Globales.mins--;
                   Globales.secs = 60; //cambiar limite de segundos 
               }

               if (Globales.mins == -1)  //si se llega a 0 los minutos 
               {
                   timer2.Stop();
                   Globales.mins = 1;  //(limite de minutos) -1
                   Globales.secs = 60;  //(limite de segundos) +1
                   //   sec.Text = "0" + secs.ToString();
                   sec.Text = Globales.secs.ToString();
                   min.Text = Globales.mins.ToString();
                   //Aqui se activa la alarma 
                   SoundPlayer player = new SoundPlayer();
                   //Aqui va el directorio del wav 
                   player.SoundLocation = "alert.wav";
                   player.Load();
                   player.PlaySync();
                   PDP2.Text = HoraActual.Text;
                   Globales.start3 = false;
                   Globales.contadorfinal2 = false;
               }

           }
           //--------------------------------------------------------------------      
     
            //-----------------------------------------------------------------
           TF.Text = PDP2.Text; // igualar el tiempo final con el ultimo dato tomado de las pruebas
          
                  
       }

        private void start2_Click(object sender, EventArgs e)
        {
            timer2.Enabled = true;
            PCP.Text = HoraActual.Text;
            Globales.start2 = true;
            PCP2.Text = "Tiempo no tomado";
        }

        private void start3_Click(object sender, EventArgs e)
        {
            timer2.Enabled = true;
            PDP.Text = HoraActual.Text;
            Globales.start3 = true;
            PDP2.Text = "Tiempo no tomado";
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            timer3.Interval = 890;  //intervalo para el temporizador 
            if (Globales.contadorfinal2 == true)
            {
                Globales.contadorfinal++;
                if (Globales.contadorfinal == 60)
                {
                    Globales.minutosfinales++;
                    Globales.contadorfinal = 0;
                }
                if (Globales.contadorfinal <= 9 && Globales.contadorfinal >= 0)  // si esta dentro de los valores de 0 y 9
                {

                    TTP.Text = Globales.minutosfinales.ToString() + ":" + "0" + Globales.contadorfinal.ToString();

                }else
                TTP.Text = Globales.minutosfinales. ToString() +":"+ Globales.contadorfinal.ToString();
            }
          

        }

        private void TTP_TextChanged(object sender, EventArgs e)
        {

        }

        public void button1_Click(object sender, EventArgs e)
        {

           // Limpiar textos 
            textBox1.Text = " ";
            textBox2.Text = " ";
            textBox3.Text = " ";
            textBox4.Text = " ";
            textBox5.Text = " ";
            IP.Text = " ";
            AE.Text = " ";
            TF.Text = " ";
            TTP.Text = " ";
            PSP.Text = " ";
            PSP2.Text = " ";
            PCP.Text = " ";
            PCP2.Text = " ";
            PDP.Text = " ";
            PDP2.Text = " ";
            Folio.Text = " ";
            timer3.Stop();
            timer2.Stop();
            Globales.mins = 1;  //(limite de minutos) -1
            Globales.secs = 60;  //(limite de segundos) +1
            Globales.start3 = false;
            Globales.start2 = false;
            Globales.start1 = false;
            Globales.contadorfinal2 = false;
            Globales.minutosfinales = 0;
            Globales.contadorfinal = 0;
            sec.Text = Globales.secs.ToString();
            min.Text = Globales.mins.ToString();
            Globales.nofolio++;
            NoFolio.Text = Globales.nofolio.ToString();
            Folio.Text = Globales.nofolio.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
             if (Folio.Text == "")
                {

                    MessageBox.Show("Escriba el número de folio");
                }
             else
             {

                 FolderBrowserDialog fbd = new FolderBrowserDialog();
                 if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                 {
                     Globales.ruta = fbd.SelectedPath;

                     System.IO.StreamWriter objWriter;
                     objWriter = new System.IO.StreamWriter(Globales.ruta + "\\" + Folio.Text + ".txt");
                     string tLine =  "";
                     string[] aryText = new string[17];
                     aryText[0] = "Nombre del Paciente: " + textBox1.Text;
                     aryText[1] = "Inicio de Prueba: " + IP.Text;
                     aryText[2] = "Arteria Encontrada: " + AE.Text;
                     aryText[3] = "Tiempo Final: " + TF.Text;
                     aryText[4] = "Tiempo Total de Prueba: " + TTP.Text;
                     aryText[5] = "------------------------------ ";
                     aryText[6] = "Prueba sin presión inicio: " + PSP.Text;
                     aryText[7] = "Prueba sin presión final: " + PSP2.Text;
                     aryText[8] = "Prueba con presión inicio: " + PCP.Text;
                     aryText[9] = "Prueba con presión final: " + PCP2.Text;
                     aryText[10] = "Prueba despues de presion inicio: " + PDP.Text;
                     aryText[11] = "Pureba despues de presion final: " + PDP2.Text;
                     aryText[12] = "----------------------------- ";
                     aryText[13] = "Presion Inicial: " + textBox2.Text;
                     aryText[14] = "Presion Final: " + textBox3.Text;
                     aryText[15] = "Temperatura Inicial: " + textBox4.Text;
                     aryText[16] = "Temperatura Final: " + textBox5.Text;
                     for (int i = 0; i < 17; i++)
                     {
                         tLine = tLine + aryText[i] + "\r\n";
                         objWriter.WriteLine(aryText[i]);
                     
                     }
                     objWriter.Close();
                     MessageBox.Show("Se ha salvado con éxito");



                 }

             }
            
         
        }
    }
}
