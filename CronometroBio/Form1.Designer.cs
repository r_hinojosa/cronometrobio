﻿namespace CronometroBio
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.HoraActual = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Folio = new System.Windows.Forms.TextBox();
            this.TTP = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TF = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.AE = new System.Windows.Forms.TextBox();
            this.IP = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PDP2 = new System.Windows.Forms.TextBox();
            this.PCP2 = new System.Windows.Forms.TextBox();
            this.PSP2 = new System.Windows.Forms.TextBox();
            this.start3 = new System.Windows.Forms.Button();
            this.start2 = new System.Windows.Forms.Button();
            this.start1 = new System.Windows.Forms.Button();
            this.PDP = new System.Windows.Forms.TextBox();
            this.PCP = new System.Windows.Forms.TextBox();
            this.PSP = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.FechaActual = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.min = new System.Windows.Forms.Label();
            this.sec = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label15 = new System.Windows.Forms.Label();
            this.NoFolio = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(293, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(355, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Estudio de la funcion endotelial";
            // 
            // HoraActual
            // 
            this.HoraActual.AutoSize = true;
            this.HoraActual.BackColor = System.Drawing.Color.Transparent;
            this.HoraActual.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HoraActual.Location = new System.Drawing.Point(42, 69);
            this.HoraActual.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HoraActual.Name = "HoraActual";
            this.HoraActual.Size = new System.Drawing.Size(124, 29);
            this.HoraActual.TabIndex = 3;
            this.HoraActual.Text = "HoraActual";
            this.HoraActual.Click += new System.EventHandler(this.HoraActual_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 132);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Inicio de Prueba. ";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.Folio);
            this.groupBox1.Controls.Add(this.TTP);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.TF);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.AE);
            this.groupBox1.Controls.Add(this.IP);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 176);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(520, 417);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tiempo A";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 18);
            this.label12.TabIndex = 18;
            this.label12.Text = "Folio";
            // 
            // Folio
            // 
            this.Folio.BackColor = System.Drawing.SystemColors.Info;
            this.Folio.Location = new System.Drawing.Point(192, 36);
            this.Folio.Name = "Folio";
            this.Folio.Size = new System.Drawing.Size(100, 26);
            this.Folio.TabIndex = 13;
            // 
            // TTP
            // 
            this.TTP.Location = new System.Drawing.Point(191, 321);
            this.TTP.Margin = new System.Windows.Forms.Padding(4);
            this.TTP.Name = "TTP";
            this.TTP.Size = new System.Drawing.Size(215, 26);
            this.TTP.TabIndex = 17;
            this.TTP.TextChanged += new System.EventHandler(this.TTP_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 330);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(153, 18);
            this.label14.TabIndex = 16;
            this.label14.Text = "Tiempo total de prueba";
            // 
            // TF
            // 
            this.TF.Location = new System.Drawing.Point(191, 254);
            this.TF.Margin = new System.Windows.Forms.Padding(4);
            this.TF.Name = "TF";
            this.TF.Size = new System.Drawing.Size(215, 26);
            this.TF.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 262);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 18);
            this.label13.TabIndex = 14;
            this.label13.Text = "Tiempo Final";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(191, 78);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(312, 26);
            this.textBox1.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(21, 78);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 18);
            this.label9.TabIndex = 12;
            this.label9.Text = "Nombre del Paciente";
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Green;
            this.button7.Location = new System.Drawing.Point(456, 188);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(47, 31);
            this.button7.TabIndex = 11;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Green;
            this.button6.Location = new System.Drawing.Point(457, 124);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(47, 30);
            this.button6.TabIndex = 10;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // AE
            // 
            this.AE.Location = new System.Drawing.Point(191, 188);
            this.AE.Margin = new System.Windows.Forms.Padding(4);
            this.AE.Name = "AE";
            this.AE.Size = new System.Drawing.Size(215, 26);
            this.AE.TabIndex = 9;
            // 
            // IP
            // 
            this.IP.Location = new System.Drawing.Point(191, 128);
            this.IP.Margin = new System.Windows.Forms.Padding(4);
            this.IP.Name = "IP";
            this.IP.Size = new System.Drawing.Size(215, 26);
            this.IP.TabIndex = 8;
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(4, 416);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(865, 167);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 201);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Arteria encontrada.";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.PDP2);
            this.groupBox2.Controls.Add(this.PCP2);
            this.groupBox2.Controls.Add(this.PSP2);
            this.groupBox2.Controls.Add(this.start3);
            this.groupBox2.Controls.Add(this.start2);
            this.groupBox2.Controls.Add(this.start1);
            this.groupBox2.Controls.Add(this.PDP);
            this.groupBox2.Controls.Add(this.PCP);
            this.groupBox2.Controls.Add(this.PSP);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(557, 176);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(519, 417);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tiempo B";
            // 
            // PDP2
            // 
            this.PDP2.Location = new System.Drawing.Point(233, 327);
            this.PDP2.Margin = new System.Windows.Forms.Padding(4);
            this.PDP2.Name = "PDP2";
            this.PDP2.Size = new System.Drawing.Size(176, 26);
            this.PDP2.TabIndex = 13;
            // 
            // PCP2
            // 
            this.PCP2.Location = new System.Drawing.Point(233, 214);
            this.PCP2.Margin = new System.Windows.Forms.Padding(4);
            this.PCP2.Name = "PCP2";
            this.PCP2.Size = new System.Drawing.Size(176, 26);
            this.PCP2.TabIndex = 12;
            // 
            // PSP2
            // 
            this.PSP2.Location = new System.Drawing.Point(233, 106);
            this.PSP2.Margin = new System.Windows.Forms.Padding(4);
            this.PSP2.Name = "PSP2";
            this.PSP2.Size = new System.Drawing.Size(177, 26);
            this.PSP2.TabIndex = 11;
            // 
            // start3
            // 
            this.start3.BackColor = System.Drawing.Color.Green;
            this.start3.ForeColor = System.Drawing.Color.White;
            this.start3.Location = new System.Drawing.Point(427, 289);
            this.start3.Margin = new System.Windows.Forms.Padding(4);
            this.start3.Name = "start3";
            this.start3.Size = new System.Drawing.Size(76, 63);
            this.start3.TabIndex = 10;
            this.start3.Text = "START";
            this.start3.UseVisualStyleBackColor = false;
            this.start3.Click += new System.EventHandler(this.start3_Click);
            // 
            // start2
            // 
            this.start2.BackColor = System.Drawing.Color.Green;
            this.start2.ForeColor = System.Drawing.Color.White;
            this.start2.Location = new System.Drawing.Point(427, 177);
            this.start2.Margin = new System.Windows.Forms.Padding(4);
            this.start2.Name = "start2";
            this.start2.Size = new System.Drawing.Size(76, 62);
            this.start2.TabIndex = 8;
            this.start2.Text = "START";
            this.start2.UseVisualStyleBackColor = false;
            this.start2.Click += new System.EventHandler(this.start2_Click);
            // 
            // start1
            // 
            this.start1.BackColor = System.Drawing.Color.Green;
            this.start1.ForeColor = System.Drawing.Color.White;
            this.start1.Location = new System.Drawing.Point(427, 74);
            this.start1.Margin = new System.Windows.Forms.Padding(4);
            this.start1.Name = "start1";
            this.start1.Size = new System.Drawing.Size(76, 57);
            this.start1.TabIndex = 6;
            this.start1.Text = "START";
            this.start1.UseVisualStyleBackColor = false;
            this.start1.Click += new System.EventHandler(this.start1_Click);
            // 
            // PDP
            // 
            this.PDP.Location = new System.Drawing.Point(233, 289);
            this.PDP.Margin = new System.Windows.Forms.Padding(4);
            this.PDP.Name = "PDP";
            this.PDP.Size = new System.Drawing.Size(177, 26);
            this.PDP.TabIndex = 5;
            // 
            // PCP
            // 
            this.PCP.Location = new System.Drawing.Point(233, 180);
            this.PCP.Margin = new System.Windows.Forms.Padding(4);
            this.PCP.Name = "PCP";
            this.PCP.Size = new System.Drawing.Size(177, 26);
            this.PCP.TabIndex = 4;
            // 
            // PSP
            // 
            this.PSP.Location = new System.Drawing.Point(233, 74);
            this.PSP.Margin = new System.Windows.Forms.Padding(4);
            this.PSP.Name = "PSP";
            this.PSP.Size = new System.Drawing.Size(177, 26);
            this.PSP.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 289);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 18);
            this.label6.TabIndex = 2;
            this.label6.Text = "Prueba después de presión";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 188);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "Prueba con presión";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 78);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 18);
            this.label4.TabIndex = 0;
            this.label4.Text = "Prueba sin presión";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.textBox5);
            this.groupBox4.Controls.Add(this.textBox4);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.textBox3);
            this.groupBox4.Controls.Add(this.textBox2);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(13, 601);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(619, 180);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Presión y Temperatura";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(421, 110);
            this.textBox5.Margin = new System.Windows.Forms.Padding(4);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(173, 26);
            this.textBox5.TabIndex = 7;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(421, 45);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(173, 26);
            this.textBox4.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(306, 112);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 18);
            this.label11.TabIndex = 5;
            this.label11.Text = "Temperatura final";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(300, 49);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 18);
            this.label10.TabIndex = 4;
            this.label10.Text = "Temperatura incial";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(124, 110);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(165, 26);
            this.textBox3.TabIndex = 3;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(124, 47);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(168, 26);
            this.textBox2.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(21, 112);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 18);
            this.label8.TabIndex = 1;
            this.label8.Text = "Presion Final";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(21, 49);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 18);
            this.label7.TabIndex = 0;
            this.label7.Text = "Presion Inicial";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // FechaActual
            // 
            this.FechaActual.AutoSize = true;
            this.FechaActual.BackColor = System.Drawing.Color.Transparent;
            this.FechaActual.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaActual.Location = new System.Drawing.Point(676, 69);
            this.FechaActual.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FechaActual.Name = "FechaActual";
            this.FechaActual.Size = new System.Drawing.Size(134, 29);
            this.FechaActual.TabIndex = 9;
            this.FechaActual.Text = "FechaActual";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.min);
            this.groupBox5.Controls.Add(this.sec);
            this.groupBox5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(657, 601);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(419, 180);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Temprizador";
            // 
            // min
            // 
            this.min.AutoSize = true;
            this.min.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.min.Location = new System.Drawing.Point(162, 61);
            this.min.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.min.Name = "min";
            this.min.Size = new System.Drawing.Size(49, 59);
            this.min.TabIndex = 1;
            this.min.Text = "0";
            // 
            // sec
            // 
            this.sec.AutoSize = true;
            this.sec.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sec.Location = new System.Drawing.Point(203, 61);
            this.sec.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.sec.Name = "sec";
            this.sec.Size = new System.Drawing.Size(73, 59);
            this.sec.TabIndex = 0;
            this.sec.Text = "00";
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Red;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(569, 121);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 47);
            this.button1.TabIndex = 11;
            this.button1.Text = "CLEAN";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Green;
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(398, 121);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(79, 47);
            this.button2.TabIndex = 12;
            this.button2.Text = "SAVE";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(187, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 26);
            this.label15.TabIndex = 13;
            this.label15.Text = "Folio";
            // 
            // NoFolio
            // 
            this.NoFolio.AutoSize = true;
            this.NoFolio.BackColor = System.Drawing.Color.Transparent;
            this.NoFolio.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoFolio.Location = new System.Drawing.Point(200, 142);
            this.NoFolio.Name = "NoFolio";
            this.NoFolio.Size = new System.Drawing.Size(86, 26);
            this.NoFolio.TabIndex = 14;
            this.NoFolio.Text = "#deFolio";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1089, 804);
            this.Controls.Add(this.NoFolio);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.FechaActual);
            this.Controls.Add(this.HoraActual);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Time";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label HoraActual;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox AE;
        private System.Windows.Forms.TextBox IP;
        private System.Windows.Forms.TextBox PDP;
        private System.Windows.Forms.TextBox PCP;
        private System.Windows.Forms.TextBox PSP;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button start3;
        private System.Windows.Forms.Button start2;
        private System.Windows.Forms.Button start1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label FechaActual;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label sec;
        private System.Windows.Forms.TextBox TTP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox TF;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox PDP2;
        private System.Windows.Forms.TextBox PCP2;
        private System.Windows.Forms.TextBox PSP2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label min;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox Folio;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label NoFolio;
        private System.Windows.Forms.Label label15;
    }
}

